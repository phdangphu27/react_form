export const err = {};

const checkEmpty = (userInput, fieldErr, errMessage) => {
  if (!userInput) {
    err[fieldErr] = errMessage;
    return false;
  } else {
    err[fieldErr] = "";
    return true;
  }
};

const checkNumber = (userInput, fieldErr, errMessage) => {
  const reg = /^\d+$/;

  const isValid = reg.test(userInput);

  if (isValid) {
    err[fieldErr] = errMessage;
    return false;
  } else {
    err[fieldErr] = "";
    return true;
  }
};

const checkSpecialChar = (userInput, fieldErr, errMessage) => {
  const reg = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;

  const isValid = reg.test(userInput);

  if (isValid) {
    err[fieldErr] = errMessage;
    return false;
  } else {
    err[fieldErr] = "";
    return true;
  }
};

const checkRepeated = (
  userInput,
  fieldErr,
  errMessage,
  field,
  initialState
) => {
  const index = initialState.studentsArr.findIndex((student) => {
    return student[field] === userInput;
  });

  if (index === -1) {
    err[fieldErr] = "";
    return true;
  } else {
    err[fieldErr] = errMessage;
    return false;
  }
};

export const checkValid = (payload, state) => {
  let isValid;

  const id =
    checkEmpty(payload.id, "idErr", "Mã SV không được để trống") &&
    checkRepeated(payload.id, "idErr", "Mã SV không được trùng", "id", state);

  const fullName =
    checkEmpty(
      payload.fullName,
      "fullNameErr",
      "Họ và tên không được để trống"
    ) &&
    checkNumber(
      payload.fullName,
      "fullNameErr",
      "Họ và tên không được chứa số"
    ) &&
    checkSpecialChar(
      payload.fullName,
      "fullNameErr",
      "Họ và tên không được chứa ký tự đặc biệt"
    );

  const phoneNumber =
    checkEmpty(
      payload.phoneNumber,
      "phoneNumberErr",
      "Số điện thoại không được để trống"
    ) &&
    checkSpecialChar(
      payload.phoneNumber,
      "phoneNumberErr",
      "Số điện thoại không được chứa ký tự đặc biệt"
    ) &&
    checkRepeated(
      payload.phoneNumber,
      "phoneNumberErr",
      "Số điện thoại không được trùng",
      "phoneNumber",
      state
    );

  const email =
    checkEmpty(payload.email, "emailErr", "Email không được để trống") &&
    checkRepeated(
      payload.email,
      "emailErr",
      "Email không được trùng",
      "email",
      state
    );

  isValid = id && fullName && phoneNumber && email;

  return isValid;
};
