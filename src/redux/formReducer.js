import { ADD, INPUT, DELETE, EDIT, UPDATE } from "./const";
import { err, checkValid } from "../validate/validate";

const initialState = {
  student: {
    id: "",
    fullName: "",
    phoneNumber: "",
    email: "",
  },

  studentsArr: [
    {
      id: "1",
      fullName: "pdp",
      phoneNumber: "0932167450",
      email: "phdangphu27@gmail.com",
    },
  ],

  isValid: true,
  errMessage: null,
};

export const formReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case INPUT: {
      const { name: key, value } = payload.target;
      const cloneStudent = { ...state.student };
      cloneStudent[key] = value;
      return { ...state, student: cloneStudent };
    }

    case ADD: {
      const cloneArr = [...state.studentsArr];

      const valid = checkValid(payload, state);

      if (valid) {
        cloneArr.push(payload);
        return {
          ...state,
          studentsArr: cloneArr,
          isValid: true,
          errMessage: "",
        };
      } else {
        return {
          ...state,
          studentsArr: cloneArr,
          isValid: false,
          errMessage: err,
        };
      }
    }

    case EDIT: {
      const cloneStudent = { ...payload };
      return { ...state, student: cloneStudent };
    }

    case UPDATE: {
      const cloneArr = [...state.studentsArr];

      let cloneStudent = { ...state.student };

      cloneStudent = payload;

      const index = cloneArr.findIndex((student) => {
        return student.id === payload.id;
      });

      cloneArr[index] = payload;

      // const valid = checkValid(payload, state);

      // if (valid) {

      //   return {
      //     ...state,
      //     studentsArr: cloneArr,
      //     student: cloneStudent,
      //     isValid: true,
      //     errMessage: "",
      //   };
      // } else {
      //   return {
      //     ...state,
      //     studentsArr: cloneArr,
      //     student: payload,
      //     isValid: false,
      //     errMessage: err,
      //   };
      // }

      console.log({ ...state, student: cloneStudent, studentsArr: cloneArr });

      return { ...state, student: cloneStudent, studentsArr: cloneArr };
    }

    case DELETE: {
      const cloneArr = [...state.studentsArr];
      const index = cloneArr.findIndex((student) => {
        return student.id === payload;
      });
      cloneArr.splice(index, 1);
      console.log(cloneArr);
      return { ...state, studentsArr: cloneArr };
    }

    default:
      return state;
  }
};
