import "./App.css";
import Form from "./Form/Form";
import Table from "./Table/Table";

function App() {
  return (
    <div className='container'>
      <Form />
      <Table />
    </div>
  );
}

export default App;
