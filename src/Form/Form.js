import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD, INPUT, UPDATE } from "../redux/const";

class Form extends Component {
  render() {
    return (
      <form>
        <div className='form-group'>
          <label>Mã SV</label>
          <input
            value={this.props.data.id}
            onChange={(e) => {
              this.props.inputHandler(e);
            }}
            type='number'
            className='form-control'
            name='id'
            id='id'
          />
          {!this.props.isValid && (
            <span className='text-danger'>{this.props.errMessage.idErr}</span>
          )}
        </div>
        <div className='form-group'>
          <label>Họ tên</label>
          <input
            value={this.props.data.fullName}
            onChange={(e) => {
              this.props.inputHandler(e);
            }}
            type='text'
            className='form-control'
            name='fullName'
            id='fullName'
          />
          {!this.props.isValid && (
            <span className='text-danger'>
              {this.props.errMessage.fullNameErr}
            </span>
          )}
        </div>
        <div className='form-group'>
          <label>Số điện thoại</label>
          <input
            value={this.props.data.phoneNumber}
            onChange={(e) => {
              this.props.inputHandler(e);
            }}
            type='number'
            className='form-control'
            name='phoneNumber'
            id='phoneNumber'
          />
          {!this.props.isValid && (
            <span className='text-danger'>
              {this.props.errMessage.phoneNumberErr}
            </span>
          )}
        </div>
        <div className='form-group'>
          <label>Email</label>
          <input
            value={this.props.data.email}
            onChange={(e) => {
              this.props.inputHandler(e);
            }}
            type='email'
            className='form-control'
            name='email'
            id='email'
          />
          {!this.props.isValid && (
            <span className='text-danger'>
              {this.props.errMessage.emailErr}
            </span>
          )}
        </div>
        <button
          type='button'
          className='btn btn-success'
          onClick={() => {
            this.props.addHandler(this.props.data);
          }}
        >
          Thêm sinh viên
        </button>
        <button
          type='button'
          onClick={() => {
            this.props.updateHander(this.props.data);
          }}
          className='btn btn-primary'
        >
          Cập nhật
        </button>
      </form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.formReducer.student,
    isValid: state.formReducer.isValid,
    errMessage: state.formReducer.errMessage,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    inputHandler: (event) => {
      dispatch({
        type: INPUT,
        payload: event,
      });
    },

    addHandler: (obj) => {
      dispatch({
        type: ADD,
        payload: obj,
      });
    },

    updateHander: (obj) => {
      dispatch({
        type: UPDATE,
        payload: obj,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);
