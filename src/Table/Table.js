import React, { Component } from "react";
import { connect } from "react-redux";
import { DELETE, EDIT } from "../redux/const";

class Table extends Component {
  renderStudent = () => {
    return this.props.data.map((student, key) => {
      return (
        <tr key={key}>
          <td>{student.id}</td>
          <td>{student.fullName}</td>
          <td>{student.phoneNumber}</td>
          <td>{student.email}</td>
          <td>
            <button
              onClick={() => {
                this.props.editHandler(student);
              }}
              className='btn btn-warning'
            >
              EDIT
            </button>
            <button
              onClick={() => {
                this.props.deleteHandler(student.id);
              }}
              className='btn btn-danger'
            >
              DELETE
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <table className='table'>
        <thead>
          <tr>
            <td>Mã SV</td>
            <td>Họ tên</td>
            <td>Số điện thoại</td>
            <td>Email</td>
            <td>Action</td>
          </tr>
        </thead>
        <tbody>{this.renderStudent()}</tbody>
      </table>
    );
  }
}

const mapStateToProps = (state) => {
  return { data: state.formReducer.studentsArr };
};

const mapDispatchToProps = (dispatch) => {
  return {
    deleteHandler: (id) => {
      dispatch({
        type: DELETE,
        payload: id,
      });
    },
    editHandler: (obj) => {
      dispatch({
        type: EDIT,
        payload: obj,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Table);
